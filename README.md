# Cobblestone JS documentation

This is the documentation repository that applies to the general usage and patterns of Cobblestone JS. Individual plugins may have additional detail, but this is how to actually use it.

## Recipes

Right now, much of the system is described in terms of examples.

**[Recipes](recipes/index.md)**
