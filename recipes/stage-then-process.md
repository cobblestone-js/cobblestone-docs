---
title: Stage then Process
---

When Cobblestone JS was originally created, it was built with the idea of gathering data from multiple Git repositories into a single website. For example, pulling in the repos for individual novels, documentation projects, and source data, then treating everything as a single set of data to be rendered.

To do this, we split our processes into two separate steps: stage and process.

We stage the data by using various Gulp tasks to copy the results into the `build/stage` directory. Then, we perform the entire website rendering from `build/stage` into `build/dist`.

# Setup

```shell
npm install --save gulp@4 gulp-load-plugins
```

# Project Structure

* `src`
    * `pages`: Static Markdown and HTML pages to include in the website.
    * `posts`: Location of blog post entries.
* `build`
    * `dist`: Final processed output goes here.
    * `stage`: A place to gather everything in a single location.

# Files

## gulpfile.js

```javascript
var gulp = require("gulp");

// We use plugins just to avoid lots of require statements and to make it
// easier to add/remove various Gulp plugins as we go.
var plugins = require("gulp-load-plugins")();

gulp.task(
    "stage",
    function() {
        return gulp.src("src/pages/**/*")
            .pipe(gulp.dest("build/stage"));
    });

gulp.task(
    "process",
    ["stage"],
    function() {
        return gulp.src("build/stage/**/*")
            // Format the pages in the website.
    		.pipe(gulp.dest("build/dist"));
    });
```

## Spliting Staging

Using the above example, we can expand the `stage` tasks to pull from pages and posts.

```javascript
gulp.task(
    "stage:pages",
    function() {
        return gulp.src("src/pages/**/*")
            .pipe(gulp.dest("build/stage"));
    });

gulp.task(
    "stage:posts",
    function() {
        return gulp.src("src/posts/**/*")
            // Do post-specific manipulations.
            .pipe(gulp.dest("build/stage/blog"));
    });

gulp.task(
    "stage",
    gulp.parallel(
        "stage:pages",
        "stage:posts"
    ));
```

## Disconnecting Staging and Processing

Another common pattern is not to have the `process` task depend on `stage`. In some cases, staging data can be expensive or we don't want to rebuild the entire website when we are just focusing on style.

```javascript
gulp.task(
    "process",
    function() {
        return gulp.src("build/stage/**/*")
    		.pipe(gulp.dest("build/dist"));
    });

gulp.task(
    "default",
    gulp.series("stage", "process"));
```
