---
title: Directory Structure
---

With the flexibility of Gulp plugins, the project can use any arbitrary directory structure to organize the project. For these examples, we use a specific layout based on our uses of the tool.

* `ROOT`: This is the project (Git) root.
    * `src`: Contains all the source files.
        * `pages`: Static pages or ones that don't have a date component.
        * `posts`: Blog entries. This can have a number of formats based on what works for the author:
            * `YYYY-MM-DD-title.md`: Single flat structure.
            * `YYYY/MM-DD-title.md`: All posts in a single year gathered together.
            * `YYYY/MM/DD/title/index.md`: Everything in a sparse structure.
            * Posts can also be a mix of these different naming conventions to allow for flexible changes over time.
    * `build`: Contains the generated output. *[Ignored]*
        * `dist`: The final, rendered output. This is what is uploaded to the website.
        * `stage`: Many of the more complicated recipes gather all the data from multiple sources into a single place, and then format the results in a single stream to populate `dist`.
