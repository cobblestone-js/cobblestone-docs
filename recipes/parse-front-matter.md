---
title: Convert Markdown into HTML
---

# Setup

```shell
npm install --save gulp@4 gulp-load-plugins gulp-filter gulp-gray-matter
```

# Project Structure

* `src`
    * `pages`: Static Markdown and HTML pages to include in the website.
* `build`
    * `dist`: Final processed output goes here.

# Files

## gulpfile.js

```javascript
var gulp = require("gulp");

// We use plugins just to avoid lots of `require` statements and to make it
// easier to add/remove various Gulp plugins as we go.
var plugins = require("gulp-load-plugins")();

gulp.task(
    "process",
    function() {
        // There are certain points in the processing where we only want to
        // handle files that have a YAML front matter to perform operations like
        // removing future files or creating links.
        var frontMatterFilter = plugins.filter(
    		["**/*.html"],
    		{restore: true});

        // We need to return the stream which performs the actual operations.
        return gulp.src("src/pages/**/*")
            // Briefly filter out the files that don't have a YAML front matter.
            .pipe(frontMatterFilter)

            // Remove the YAML header from HTML/Markdown files and put it into
            // the `page` property of the stream.
    		.pipe(plugins.grayMatter({property: "page"}))

            // Perform front matter tasks, such as converting Markdown to HTML.

            // Once we are done processing the front matter files, put in
            // everything else back in so we have a fully populated stream.
    		.pipe(frontMatterFilter.restore)

            // Write out the results to the final folder.
    		.pipe(gulp.dest("build/dist"))
    });
```
