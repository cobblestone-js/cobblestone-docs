---
title: Cobblestone JS Recipes
---

All of the examples given here have the same environment and layout:

* [Directory Structure](directory-structure.md)

# Common Patterns

* [Stage then Process](stage-then-process.md): To break apart gathering files into multiple steps, many of our examples combine files into `build/stage` and then use that to convert into the final website as a single Gulp pipe.
* [Convert Markdown to HTML](convert-markdown-to-html.md): Convert Markdown files (ending in `.md` and `.markdown`) into HTML.
