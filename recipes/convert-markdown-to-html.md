---
title: Convert Markdown into HTML
---

Most of our posts are written in Markdown, however we need to convert them to HTML for styling.

# Setup

```shell
npm install --save gulp@4 gulp-load-plugins gulp-filter gulp-markdown
```

# Project Structure

* `src`
    * `pages`: Static Markdown and HTML pages to include in the website.
* `build`
    * `dist`: Final processed output goes here.

# Files

## gulpfile.js

```javascript
var gulp = require("gulp");

// We use plugins just to avoid lots of `require` statements and to make it
// easier to add/remove various Gulp plugins as we go.
var plugins = require("gulp-load-plugins")();

gulp.task(
    "process",
    function() {
        // A filter to isolate Markdown-specific changes.
        var markdownFilter = plugins.filter(
    		["**/*.md", "**.markdown"],
    		{restore: true});

        // We need to return the stream which performs the actual operations.
        return gulp.src("src/pages/**/*")
            // Convert Markdown files (`.md` and `.markdown`) into HTML.
            .pipe(markdownFilter)
            .pipe(plugins.markdown())
            .pipe(markdownFilter.restore)

            // Write out the results to the final folder.
    		.pipe(gulp.dest("build/dist"))
    });
```
